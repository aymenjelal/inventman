﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public DateTime Date { get; set; }
        public int Tresh { get; set; }

        MySqlConnection conn = DBUtils.GetDBConnection();

        public ProductModel()
        {

        }

        public ProductModel(string name, string brand, int quantity, decimal price, DateTime date, int tresh)
        {
            Name = name;
            Brand = brand;
            Quantity = quantity;
            Price = price;
            Date = date;
            Tresh = tresh;
        }

        public ProductModel(int id, string name, string brand, int quantity, decimal price, DateTime date, int tresh)
        {
            Id = id;
            Name = name;
            Brand = brand;
            Quantity = quantity;
            Price = price;
            Date = date;
            Tresh = tresh;
        }


        public ProductModel addProductToTable(string name, string brand, int quantity, decimal price, int tresh)
        {

            string formatForMysql = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = string.Format("INSERT INTO products(name,brand,quantity,price,min_tresh,date) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')", name, brand, quantity, price, tresh, formatForMysql);
            Console.WriteLine("Queryy " + query);
            MySqlCommand cmd = new MySqlCommand(query, conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            int id = (int) cmd.LastInsertedId;


            ProductModel newProduct = new ProductModel(id, name, brand, quantity, price, DateTime.Now,tresh);
            conn.Close();

            return newProduct;

        }

        public void addProduct(OrderModel order)
        {
            ProductModel product = new ProductModel();
            product= product.getProduct(order.ProductName);
            Console.WriteLine("Inside add product");
            

            if (product != null)
            {
                int newQuantity= product.Quantity + order.Quantity; 
                UpdateQuantity(product.Id, newQuantity,order.Price);
                    
                

            }
            else
            {
                addProductToTable(order.ProductName, order.ProductBrand, order.Quantity, order.Price, order.Tresh);
            }
        }

        public List<ProductModel> getProducts()
        {
            List<ProductModel> products = new List<ProductModel>();
            string query = "SELECT * FROM products";

            MySqlCommand cmd = new MySqlCommand(query, conn);

            conn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = (int)reader["id"];
                string name = reader["name"].ToString();
                string brand = reader["brand"].ToString();
                int quantity = (int)reader["quantity"];
                decimal price = (decimal)reader["price"];
                int tresh = (int)reader["min_tresh"];
                DateTime date = (DateTime)reader["date"];

                
                ProductModel product = new ProductModel(id, name, brand, quantity, price, date,tresh);

                products.Add(product);             
            }

            conn.Close();
            return products;
        }

        public ProductModel getProduct(string name)
        {
            string query = string.Format("SELECT * FROM products where name='{0}'", name);

            MySqlCommand cmd = new MySqlCommand(query, conn);

            conn.Open();
            ProductModel product=null;

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = (int)reader["id"];
                string pname = reader["name"].ToString();
                string brand = reader["brand"].ToString();
                int quantity = (int)reader["quantity"];
                decimal price = (decimal)reader["price"];
                int tresh = (int)reader["min_tresh"];
                DateTime date = (DateTime)reader["date"];


                product = new ProductModel(id, pname, brand, quantity, price, date,tresh);

                
            }

            

            conn.Close();
            return product;

        }

        public void UpdateQuantity(int id, int quantity,decimal price)
        {
            try
            {
                string query = string.Format("UPDATE products SET quantity={0}, price={1} where id ={2}", quantity,price,id);
                MySqlCommand cmd = new MySqlCommand(query, conn);
                conn.Open();
                cmd.ExecuteNonQuery();




                conn.Close();

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);

            }
        }

        public void UpdateQuantity(int id, int quantity)
        {
            try
            {
                string query = string.Format("UPDATE products SET quantity={0} where id={1}", quantity, id);
                MySqlCommand cmd = new MySqlCommand(query, conn);
                conn.Open();
                cmd.ExecuteNonQuery();

                conn.Close();

            }

            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        
    }
}
