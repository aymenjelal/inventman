﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.Models
{
    public class OrderModel
    {

        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProductBrand { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public DateTime OrderDate { get; set; }
        public int Pending { get; set; }
        public int Tresh { get; set; }

        MySqlConnection conn = DBUtils.GetDBConnection();
        ProductModel order = new ProductModel();

        public OrderModel(int id, string productName, string productBrand, int quantity, decimal price, DateTime orderDate, int tresh, int pending)
        {
            Id = id;
            ProductName = productName;
            ProductBrand = productBrand;
            Quantity = quantity;
            Price = price;
            OrderDate = orderDate;
            Tresh = tresh;
            Pending = pending;
        }

        public OrderModel()
        {
        }

        public void placeOrder(string productName,string productBrand,int quantity, decimal price,int tresh,DateTime orderDate,int pending) 
        {
            //System.Globalization.CultureInfo.CurrentCulture.ClearCachedData();
            string formatForMysql = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Console.WriteLine(formatForMysql);

            try
            {
                string query = string.Format("INSERT INTO orders(product_name,product_brand,quantity,price,date,min_tresh,pending) VALUES ('{0}','{1}','{2}','{3}','{4}',{5},{6})", productName, productBrand, quantity, price, formatForMysql,tresh, pending);
                MySqlCommand cmd = new MySqlCommand(query, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                int id = (int)cmd.LastInsertedId;


                OrderModel newOrder = new OrderModel(id, productName, productBrand, quantity, price, orderDate,tresh, pending);
                conn.Close();

                

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);

            }

        }

        public List<OrderModel> getPendingOrders()
        {
            List<OrderModel> orders = new List<OrderModel>();
            string query = "SELECT * FROM orders WHERE pending=1";

            MySqlCommand cmd = new MySqlCommand(query, conn);

            conn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = (int)reader["id"];
                string productName = reader["product_name"].ToString();
                string productBrand = reader["product_brand"].ToString();
                int quantity = (int)reader["quantity"];
                decimal price = (decimal)reader["price"];
                
                DateTime date = (DateTime)reader["date"];
                int tresh = (int)reader["min_tresh"];


                OrderModel order = new OrderModel(id, productName, productBrand, quantity, price, date,tresh,1);

                orders.Add(order);
            }

            conn.Close();
            return orders;
        }

        public void RemovePending(int id )
        {
            try
            {
                string query = string.Format("UPDATE orders SET pending=0 where id ={0}",id);
                MySqlCommand cmd = new MySqlCommand(query, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                


                
                conn.Close();

            }

            catch(Exception ex)
            {
                Console.WriteLine(ex);

            }
        }

        
    }
}
