﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.Models
{
    public class SalesModel
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal SalePrice { get; set; }
        public DateTime SaleDate { get; set; }
        public string Employee { get; set; }
        public string Buyer { get; set; }

        MySqlConnection conn = DBUtils.GetDBConnection();


        public SalesModel()
        {

        }

        public SalesModel(int id, string productName, int quantity, decimal price, DateTime saleDate, string employee, string buyer)
        {
            Id = id;
            ProductName = productName;
            Quantity = quantity;
            SalePrice = price;
            SaleDate = saleDate;
            Employee = employee;
            Buyer = buyer;
        }

        public SalesModel addSale(string productName, int quantity, decimal totalPrice, string employee, string buyer)
        {
            string formatForMysql = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = string.Format("INSERT INTO sales(product_name,quantity,price,sale_date,employee,buyer) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')", productName, quantity, totalPrice, formatForMysql, employee, buyer);
            Console.WriteLine("Queryy " + query);
            MySqlCommand cmd = new MySqlCommand(query, conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            int id = (int)cmd.LastInsertedId;

            SalesModel newSale = new SalesModel(id, productName, quantity, totalPrice, DateTime.Now, employee, buyer);

            
            conn.Close();

            return newSale;
        }

        public List<SalesModel> getsales()
        {
            List<SalesModel> sales = new List<SalesModel>();
            string query = "SELECT * FROM sales";

            MySqlCommand cmd = new MySqlCommand(query, conn);

            conn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = (int)reader["id"];
                string name = reader["product_name"].ToString();
                
                int quantity = (int)reader["quantity"];
                decimal price = (decimal)reader["price"];
               
                DateTime date = (DateTime)reader["sale_date"];
                string employee = reader["employee"].ToString();
                string buyer = reader["buyer"].ToString();


               SalesModel sale = new SalesModel(id, name, quantity, price, date,employee,buyer);

                sales.Add(sale);
            }

            conn.Close();
            return sales;
        }

        public List<SalesModel> getSalesUsingYear(string sDate)
        {
            List<SalesModel> sales = new List<SalesModel>();


            string query = string.Format("SELECT * FROM sales WHERE sale_date LIKE '{0}%'",sDate);

            MySqlCommand cmd = new MySqlCommand(query, conn);

            conn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = (int)reader["id"];
                string name = reader["product_name"].ToString();

                int quantity = (int)reader["quantity"];
                decimal price = (decimal)reader["price"];

                DateTime date = DateTime.Parse(reader["sale_date"].ToString());
                string employee = reader["employee"].ToString();
                string buyer = reader["buyer"].ToString();

               



                SalesModel sale = new SalesModel(id, name, quantity, price, date, employee, buyer);
                Console.WriteLine("date issss" + DateTime.Parse(sale.SaleDate.ToString()).ToString());
                sales.Add(sale);
            }

            conn.Close();
            return sales;

        }

    }
}
