﻿using Caliburn.Micro;
using InventMan.Models;
using InventMan.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.ViewModels
{
    class ShellViewModel: Conductor<object>
    {
     
        public UserModel user;

        private string _userName;
        private string _password;

        //private readonly IWindowManager windowManager;
        //private readonly HomeViewModel homeViewModel;


        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyOfPropertyChange(() => Password);
            }
        }

        //public ShellViewModel(HomeViewModel homeViewModel, IWindowManager windowManager)
        //{
        //    this.homeViewModel = homeViewModel;
        //    this.windowManager = windowManager;
        //}

        IWindowManager manager = new WindowManager();


        public void Login(string userName)
        {
            user = new UserModel();
            user = user.getUser(userName);

            if (user == null)
            {
                Console.WriteLine("nulll");
            }
            else
            {
                if (user.Password.Equals(this.Password))
                {
                    Console.WriteLine("hereee");
                    //HomeViewModel home = new HomeViewModel();
                    if (user.Type==1)
                    {
                        manager.ShowWindow(new HomeViewModel(user), null, null);
                        TryClose();
                    }

                    else
                    {
                        manager.ShowWindow(new AdminViewModel(user), null, null);
                        TryClose();
                    }
                    

                    
                }

            }

        }

        public void Close()
        {
            TryClose();
        }
    }
}
