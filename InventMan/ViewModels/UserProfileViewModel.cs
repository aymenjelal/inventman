﻿using Caliburn.Micro;
using InventMan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.ViewModels
{
    public class UserProfileViewModel : Conductor<object>
    {
        private string _id;
        private string _firstName;
        private string _lastName;
        private string _passowrd;
        private string _rePassowrd;
        private string _email;
        private int _type;
        private string _match;
        private string _success;

        UserModel user = new UserModel();



        public string Id {
            get { return _id; }
            set
            {
                _id = value;
                NotifyOfPropertyChange(() => Id);
            }
        }

        public string FirstName {
            get { return _firstName; }
            set
            {
                _firstName = value;
                
                NotifyOfPropertyChange(() => FirstName);
            }
        }

        public string LastName {
            get { return _lastName; }
            set
            {
                _lastName = value;
                NotifyOfPropertyChange(() => LastName);
            }
        }

        public string Password {
            get { return _passowrd; }
            set
            {
                _passowrd = value;
                
                NotifyOfPropertyChange(() => Password);
            }
        }

        public string RePassword
        {
            get { return _rePassowrd; }
            set
            {
                _rePassowrd = value;
                
                NotifyOfPropertyChange(() => RePassword);
                CheckPasswordMatch(Password, RePassword);
            }
        }

        public string Email {
            get { return _email; }
            set
            {
                _email = value;
                NotifyOfPropertyChange(() => Email);
            }
        }

        public int Type {
            get { return _type; }
            set
            {
                _type = value;
                NotifyOfPropertyChange(() => Type);
            }
        }
        
        public string Match
        {
            get { return _match; }
            set {
                _match = value;
                NotifyOfPropertyChange(() => Match);
            }
        }

        public string Success
        {
            get { return _success; }
            set
            {
                _success = value;
                NotifyOfPropertyChange(() => Success);
            }
        }

        public UserProfileViewModel(UserModel user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Password = user.Password;
            RePassword = "";
            Email = user.Email;
            Match = "";
            Success = "";
            this._type = user.Type;

        }

        public bool CheckPasswordMatch(string password,string reEnter)
        {
            
            if (password.Equals(reEnter)){
                Match = "Passwords Match";
                return true;
            }
            else
            {
                Match="Passwords dont Match";
                return false;
            }
        }

        public void UpdateProfile(string firstName, string lastName, string password,string rePassword, string email)
        {
            if (CheckPasswordMatch(Password,RePassword))
            {
                Console.WriteLine("IdDDD " + Id);
                user.update(Id, firstName, lastName, Password, email);
                Success = "Updated Successfully";
                

            }

            else
            {
                
                Success = "Profile Not updated";
            }
        }
    }
}
