﻿using Caliburn.Micro;
using InventMan.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace InventMan.ViewModels
{
    public class SalesViewModel:Conductor<object>
    {
        private int _id;
        private string _productName;
        private string _quantity;
        private string _salePrice;
        private DateTime _saleDate;
        private string _employee;
        private string _buyer;
        private string _success;

        private ObservableCollection<ProductModel> _products = new ObservableCollection<ProductModel>();
        private ProductModel _selectedProduct;
        private ObservableCollection<ChartModel> _productChart = new ObservableCollection<ChartModel>();
        private string _TextSearch;
        ProductModel product = new ProductModel();
        SalesModel sale = new SalesModel();

        private ListCollectionView _view;

        public SalesViewModel(UserModel user)
        {
            getProducts();
            this._view = new ListCollectionView(this.Products);
            this._employee = user.Id;
        }
        


        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyOfPropertyChange(() => Id);
            }
        }
        public string ProductName {
            get {return _productName; }
            set
            {
                _productName = value;
                NotifyOfPropertyChange(() => ProductName);
            }
        }
        public string Quantity {
            get { return _quantity; }
            set
            {
                _quantity = value;
                NotifyOfPropertyChange(() => Quantity);
            }
        }
        public string SalePrice {
            get { return _salePrice; }
            set {
                _salePrice = value;
                NotifyOfPropertyChange(() => SalePrice);
            }
        }
        public DateTime SaleDate {
            get { return _saleDate; }
            set
            {
                _saleDate = value;
                NotifyOfPropertyChange(() => SaleDate);
            }
        }
        public string Employee {
            get { return _employee; }
            set
            {
                _employee = value;
                NotifyOfPropertyChange(() => Employee);
            }
        }
        public string Buyer {
            get { return _buyer; }
            set
            {
                _buyer = value;
                NotifyOfPropertyChange(() => Buyer);
            }
        }

        public string Success
        {
            get { return _success; }
            set
            {
                _success = value;
                NotifyOfPropertyChange(() => Success);
            }
        }

        public ICollectionView View
        {
            get { return this._view; }

        }

        
        public string TextSearch
        {
            get { return _TextSearch; }
            set
            {
                _TextSearch = value;
                NotifyOfPropertyChange(() => TextSearch);

                if (String.IsNullOrEmpty(value))
                {
                    //getProducts();
                    View.Filter = null;

                }

                if (!string.IsNullOrEmpty(value))
                {
                    Console.WriteLine(value);
                    View.Filter = new Predicate<object>(o => ((ProductModel)o).Name.StartsWith(value));

                }


            }
        }

        public ObservableCollection<ProductModel> Products
        {
            get { return _products; }
            set { _products = value; NotifyOfPropertyChange(() => Products); }
        }

        public ProductModel SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                NotifyOfPropertyChange(() => SelectedProduct);

                //CalculatePercentage(SelectedProduct);
            }
        }

        public ObservableCollection<ChartModel> ProductChart
        {
            get { return _productChart; }
            set
            {
                _productChart = value;
                NotifyOfPropertyChange(() => ProductChart);


            }
        }

        public void getProducts()
        {

            List<ProductModel> productList = new List<ProductModel>();

            productList = product.getProducts();



            for (int i = 0; i < productList.Count; i++)
            {

                Products.Add(productList[i]);


            }
        }

        public void ProcessSale()
        {
          
            
            if (SelectedProduct != null)
            {

                if (SelectedProduct.Quantity <int.Parse(Quantity))
                {
                    Success = "Quanity of selected product is less";
                }
                else
                {
                    ProductName = SelectedProduct.Name;


                    Console.WriteLine("Product Name" + ProductName);
                    Console.WriteLine("Quantity " + int.Parse(Quantity));
                    Console.WriteLine("Sale Price " + decimal.Parse(SalePrice));

                    Console.WriteLine("Employeee " + Employee);
                    Console.WriteLine("Buyer " + Buyer);
                    sale.addSale(ProductName, int.Parse(Quantity), decimal.Parse(SalePrice), Employee, Buyer);

                    int newQuantity = SelectedProduct.Quantity - int.Parse(Quantity);

                    
                    product.UpdateQuantity(SelectedProduct.Id, newQuantity);


                    clearObservaleCollection(Products);

                    for(int i=0; i < Products.Count(); i++)
                    {
                        Console.WriteLine("Prouduct " + Products[i]);
                    }
                    clearListView(_view);

                    for(int j=0; j<_view.Count; j++)
                    {
                        Console.WriteLine("View " + _view.GetItemAt(j).ToString());
                    }


                    getProducts();

                    _view= new ListCollectionView(Products);

                }
                

            }

            else
            {
                Success = "No Product has been selected from the table";
            }
            

        }

        public void clearObservaleCollection (ObservableCollection<ProductModel> opm)
        {
            opm.Clear();
        }

        public void clearListView( ListCollectionView view)
        {
            
            for(int i=0; i<view.Count; i++)
            {
                Console.WriteLine("inside remove _view " + view.GetItemAt(i).ToString());
                view.RemoveAt(i);
            }
        }





    }
}
