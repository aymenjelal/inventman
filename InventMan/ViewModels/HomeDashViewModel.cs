﻿using Caliburn.Micro;
using InventMan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.ViewModels
{
    class HomeDashViewModel:Conductor<object>
    {
        private string _element;
        private double _percent;
        private List<ChartModel> _chartList= new List<ChartModel>();
        ProductModel product = new ProductModel();
        SalesModel sales = new SalesModel();
        private List<ChartModel> _barChartList = new List<ChartModel>();

        public string Element
        {
            get { return _element; }
            set { _element = value; }
        }

        public double Percent
        {
            get { return _percent; }
            set { _percent = value; }
        }

        public List<ChartModel> ChartList
        {
            get { return _chartList; }
            set {
                _chartList = value;
                NotifyOfPropertyChange(() => ChartList);
            }
        }

        public List<ChartModel> BarChartList
        {
            get { return _barChartList; }
            set
            {
                _barChartList = value;
                NotifyOfPropertyChange(() => BarChartList);
            }
        }


        public HomeDashViewModel()
        {
            AddProductsToCharList(product.getProducts());
            BarChartList = calculateMonthlySalesChart();
        }

        public void AddProductsToCharList(List<ProductModel> products)
        {
            for(int i=0; i<products.Count(); i++)
            {
                ChartList.Add(new ChartModel(products[i].Name, calculatePercentage(products[i], products)));
            }

        }

        public double calculatePercentage(ProductModel calProduct, List<ProductModel> products)
        {
            double percent;
            int quant;
            int totalQuant=0;

            for(int i=0; i < products.Count(); i++)
            {
                quant = products[i].Quantity;
                totalQuant = totalQuant + quant;
            }

            percent = calProduct.Quantity * 100 / totalQuant;
            return percent;

        }


        public List<ChartModel> calculateMonthlySalesChart()
        {
            List<ChartModel> barChart = new List<ChartModel>();
            List<SalesModel> barSales = new List<SalesModel>();

            DateTime currentDateTime = DateTime.Now;

            string[] dateList = currentDateTime.ToString().Split('/');
            string[] yearAndTime = dateList[2].Split(' ');

            Console.WriteLine("Yer is " + yearAndTime[0]);

            barSales = sales.getSalesUsingYear(yearAndTime[0]);
            Dictionary<string, decimal> monthSale = new Dictionary<string, decimal>();


            foreach (SalesModel sm in barSales)
            {
                
                DateTime saleDate = sm.SaleDate;
                string[] saleDateList = sm.SaleDate.ToString().Split('/');
                
                string saleMonth = saleDateList[0];

                



                if (monthSale.TryGetValue(saleMonth,out decimal value))
                {
                    value = value + (sm.Quantity * sm.SalePrice);

                }
                else
                {
                    monthSale.Add(saleMonth, (sm.Quantity * sm.SalePrice));
                }


            }

            foreach (KeyValuePair<string, decimal> item in monthSale)
            {
                ChartModel cm = new ChartModel(item.Key,item.Value);

                barChart.Add(cm);

            }


            return barChart;





        }

       
    }
}
