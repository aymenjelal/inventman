﻿using Caliburn.Micro;
using InventMan.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace InventMan.ViewModels
{
    public class PendingOrderViewModel:Conductor<object>
    {
        private int _id;
        private string _productName;
        private string _productBrand;
        private int _quantity;
        private decimal _price;
        private DateTime _orderDate;
        private int _pending;
        private int _tresh;
        private ObservableCollection<OrderModel> _orders = new ObservableCollection<OrderModel>();
        private OrderModel _selectedOrder;

        OrderModel order = new OrderModel();
        ProductModel product = new ProductModel();

        public PendingOrderViewModel()
        {
            getPendingOrders();
        }

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyOfPropertyChange(() => Id);
            }
        }

        public string ProductName
        {
            get { return _productName; }
            set
            {
                _productName = value;
                NotifyOfPropertyChange(() => ProductName);
            }
        }

        public string ProductBrand
        {
            get { return _productBrand; }
            set
            {
                _productBrand = value;
                NotifyOfPropertyChange(() => ProductBrand);
            }

        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                NotifyOfPropertyChange(() => Quantity);
            }
        }

        public decimal Price
        {
            get { return _price; }
            set
            {
                _price = value;
                NotifyOfPropertyChange(() => Price);
            }
        }

        public DateTime OrderDate
        {
            get { return _orderDate; }
            set
            {
                _orderDate = value;
                NotifyOfPropertyChange(() => OrderDate);
            }
        }

        public int Pending
        {
            get { return _pending; }
            set
            {
                _pending = value;
                NotifyOfPropertyChange(() => Pending);
            }
        }

        public int Tresh
        {
            get { return _tresh; }
            set
            {
                _tresh = value;
                NotifyOfPropertyChange(() => Tresh);
            }
        }

        public ObservableCollection<OrderModel> Orders
        {
            get { return _orders; }
            set { _orders = value; NotifyOfPropertyChange(() => Orders); }
        }

        public OrderModel SelectedOrder
        {
            get { return _selectedOrder; }
            set
            {
                _selectedOrder = value;
                if (SelectedOrder != null)
                {
                    Console.WriteLine(SelectedOrder.ToString());
                }
                
                NotifyOfPropertyChange(() => SelectedOrder);
            }
        }

        public void getPendingOrders()
        {

            List<OrderModel> orderList = new List<OrderModel>();

            orderList = order.getPendingOrders();



            for (int i = 0; i < orderList.Count; i++)
            {
                //Console.WriteLine(orderList[i].Name);
                Orders.Add(orderList[i]);


            }
        }

        public void ItemSelected(Object sender, MouseButtonEventArgs e)
        {
            TryClose();
        }

        public void RemovePending()
        {
            order.RemovePending(SelectedOrder.Id);
            product.addProduct(SelectedOrder);

            Orders.Remove(SelectedOrder);
        }

        public void DeletePending()
        {
            order.RemovePending(SelectedOrder.Id);
         

            Orders.Remove(SelectedOrder);
        }

        


    }
}
