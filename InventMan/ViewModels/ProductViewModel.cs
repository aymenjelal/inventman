﻿using Caliburn.Micro;
using InventMan.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace InventMan.ViewModels
{
    public class ProductViewModel:Conductor<object>, INotifyPropertyChanged
    {
        private int _id;
        private string _name;
        private string _brand;
        private int _quantity;
        private decimal _price;
        private DateTime _date;
        private int _tresh;
        private ObservableCollection<ProductModel> _products = new ObservableCollection<ProductModel>();
        private ProductModel _selectedProduct;
        private ObservableCollection<ChartModel> _productChart = new ObservableCollection<ChartModel>();
        ProductModel product = new ProductModel();
        


        public ProductViewModel()
        {
            getProducts();
            this._view = new ListCollectionView(this.Products);
            
            
            
        }

        private string _TextSearch;
        public string TextSearch
        {
            get { return _TextSearch; }
            set
            {
                _TextSearch = value;
                NotifyOfPropertyChange(() => TextSearch);

                if (String.IsNullOrEmpty(value))
                {
                    //getProducts();
                    View.Filter = null;

                }

                if (!string.IsNullOrEmpty(value))
                {
                    Console.WriteLine(value);
                    View.Filter = new Predicate<object>(o => ((ProductModel)o).Name.StartsWith(value));

                }
                    
                   
            }
        }

        private ListCollectionView _view;
        public ICollectionView View
        {
            get { return this._view; }
            
        }


        public int Id
        {
            get { return _id; }
            set {
                _id = value;
                NotifyOfPropertyChange(() => Id);
            }
        }

        public string Name
        {
            get { return _name; }
            set {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        private string Brand
        {
            get { return _brand; }
            set {
                _brand = value;
                NotifyOfPropertyChange(() => Brand);
            }

        }

        private int Quantity
        {
            get { return _quantity; }
            set {
                _quantity = value;
                NotifyOfPropertyChange(() => Quantity);
            }
        }

        private decimal Price
        {
            get { return _price; }
            set {
                _price = value;
                NotifyOfPropertyChange(() => Price);
            }
        }

        private DateTime Date
        {
            get { return _date; }
            set {
                _date = value;
                NotifyOfPropertyChange(() => Date);
            }
        }

        public int Tresh
        {
            get { return _tresh; }
            set
            {
                _tresh = value;
                NotifyOfPropertyChange(() => Tresh);
            }
        }

        




        public ObservableCollection<ProductModel> Products
        {
            get { return _products; }
            set { _products = value; NotifyOfPropertyChange(() => Products); }
        }

        public ProductModel SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                NotifyOfPropertyChange(() => SelectedProduct);
                
                CalculatePercentage(SelectedProduct);
            }
        }

        public ObservableCollection<ChartModel> ProductChart
        {
            get { return _productChart; }
            set
            {
                _productChart = value;
                NotifyOfPropertyChange(() => ProductChart);
                

            }
        }

        

        public void getProducts()
        {
            
            List<ProductModel> productList = new List<ProductModel>();

            productList = product.getProducts();

            

            for(int i=0; i < productList.Count; i++)
            {
                
                Products.Add(productList[i]);
                

            }
        }


        public void ChangeList()
        {
            Products.Add(new ProductModel { Id = 5, Name = "x", Brand = "y", Quantity = 40, Price = 10, Date = DateTime.Now });
        }

        

        public void ButtonCheck()
        {
            Console.WriteLine("Button pressed");
        }

       

        public void ActivateAdd()
        {
            Console.WriteLine("in activate add");
            
            ActivateItem(new AddProductViewModel());
        }

        public void CalculatePercentage(ProductModel product)
        {
            ChartModel chart = new ChartModel();
            Console.WriteLine("inside Calculate Percentage");

            double percent;
            if (product != null)
            {
                Console.WriteLine("Selected Product is " + product.ToString());
                percent = (product.Quantity - product.Tresh) * 100 /product.Quantity ;
                chart.Element = product.Name;
                chart.Percent = percent;
                if (ProductChart.Count == 5)
                {
                    ProductChart.RemoveAt(0);
                    ProductChart.Add(chart);

                }
                else
                {
                    ProductChart.Add(chart);

                }
                

            }


            Console.WriteLine("Percenttt " + chart.Percent);

            


        }

       


    }
}
