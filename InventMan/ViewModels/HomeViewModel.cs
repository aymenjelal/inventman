﻿using Caliburn.Micro;
using InventMan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.ViewModels
{
    public class HomeViewModel: Conductor<object>
    {
        IWindowManager manager = new WindowManager();
        UserModel user = new UserModel();
        public HomeViewModel(UserModel user)
        {
            this.user = user;
            ActivateItem(new HomeDashViewModel());

        }
        public void LoadProductContent()
        {
            Console.WriteLine("inside load product content");
            
            ActivateItem(new ProductViewModel());
        }

        public void LoadPlaceOrder()
        {
            ActivateItem(new PlaceOrderViewModel());
        }

        public void LoadPendingOrders()
        {
            ActivateItem(new PendingOrderViewModel());

        }

        public void LoadUserProfile()
        {
            ActivateItem(new UserProfileViewModel(user));
        }

        public void LoadDashContent()
        {
            ActivateItem(new HomeDashViewModel());
        }

        public void LoadSalesContent()
        {
            ActivateItem(new SalesViewModel(user));
        }

        public void LoadReports()
        {
            ActivateItem(new ReportsViewModel());
        }

        public void Logout()
        {
            manager.ShowWindow(new ShellViewModel(), null, null);
            TryClose();
        }




    }
}
