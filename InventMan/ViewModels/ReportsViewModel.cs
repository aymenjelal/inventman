﻿using Caliburn.Micro;
using InventMan.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace InventMan.ViewModels
{
    public class ReportsViewModel : Conductor<object>
    {
        private ObservableCollection<Combo> _comboChoices = new ObservableCollection<Combo>();
        private ObservableCollection<Combo> _comboChoices2 = new ObservableCollection<Combo>();
        private Combo _selectedComboChoice = new Combo();
        private Combo _selectedComboChoice2 = new Combo();
        private List<ChartModel> _barChartList = new List<ChartModel>();
        private List<ChartModel> _chartList = new List<ChartModel>();

        SalesModel sales = new SalesModel();
        ProductModel product = new ProductModel();

        public ReportsViewModel()
        {
            ComboChoices.Add(new Combo("product monthly sales"));
            ComboChoices.Add(new Combo("product yearly sales"));
            //ComboChoices.Add(new Combo("Employee monthly sales"));

            SelectedComboChoice = new Combo("product monthly sales");


            ComboChoices2.Add(new Combo("products in stock"));
            ComboChoices2.Add(new Combo("Ordered products"));

            SelectedComboChoice2 = new Combo("products in stock");

        }

        public ObservableCollection<Combo> ComboChoices
        {
            get { return _comboChoices; }
            set
            {
                _comboChoices = value;
                NotifyOfPropertyChange(() => ComboChoices);
            }
        }

        public ObservableCollection<Combo> ComboChoices2
        {
            get { return _comboChoices2; }
            set
            {
                _comboChoices2 = value;
                NotifyOfPropertyChange(() => ComboChoices2);
            }
        }
        public List<ChartModel> ChartList
        {
            get { return _chartList; }
            set
            {
                _chartList = value;
                NotifyOfPropertyChange(() => ChartList);
            }
        }

        public List<ChartModel> BarChartList
        {
            get { return _barChartList; }
            set
            {
                _barChartList = value;
                NotifyOfPropertyChange(() => BarChartList);
            }
        }

        public Combo SelectedComboChoice
        {
            get { return _selectedComboChoice; }
            set
            {
                _selectedComboChoice = value;
                NotifyOfPropertyChange(() => SelectedComboChoice);
                Console.WriteLine("Selected combo is " + SelectedComboChoice.Choice);

                if (SelectedComboChoice.Choice.Equals("product yearly sales"))
                {
                    BarChartList = calculateYearlySalesChart();

                }
                else if(SelectedComboChoice.Choice.Equals("product monthly sales"))
                {
                    Console.WriteLine("selected ssssss");
                    BarChartList = calculateMonthlySalesChart();
                }
                else if(SelectedComboChoice.Choice.Equals("Employee monthly sales"))
                {

                }
                Console.WriteLine("          "+SelectedComboChoice.Choice);
            }
        }

        public Combo SelectedComboChoice2
        {
            get { return _selectedComboChoice2; }
            set
            {
                _selectedComboChoice2 = value;
                NotifyOfPropertyChange(() => SelectedComboChoice2);
                Console.WriteLine("Selected combo2 is " + SelectedComboChoice2.Choice);

                
                if (SelectedComboChoice2.Choice.Equals("products in stock"))
                {
                    ChartList = calculateProductInStock();

                }
                else if (SelectedComboChoice2.Choice.Equals("product monthly sales"))
                {
                    Console.WriteLine("selected ssssss");
                    BarChartList = calculateMonthlySalesChart();
                }
                else if (SelectedComboChoice2.Choice.Equals("Employee monthly sales"))
                {

                }
                
            }
        }

        public List<ChartModel> calculateMonthlySalesChart()
        {
            List<ChartModel> barChart = new List<ChartModel>();
            List<SalesModel> barSales = new List<SalesModel>();

            DateTime currentDateTime = DateTime.Now;

            string[] dateList = currentDateTime.ToString().Split('/');
            string[] yearAndTime = dateList[2].Split(' ');

            Console.WriteLine("Yer is " + yearAndTime[0]);

            barSales = sales.getSalesUsingYear(yearAndTime[0]);
            Dictionary<string, decimal> monthSale = new Dictionary<string, decimal>();


            foreach (SalesModel sm in barSales)
            {

                DateTime saleDate = sm.SaleDate;
                string[] saleDateList = sm.SaleDate.ToString().Split('/');

                string saleMonth = saleDateList[0];


                if (monthSale.TryGetValue(saleMonth, out decimal value))
                {
                    value = value + (sm.Quantity * sm.SalePrice);

                }
                else
                {
                    monthSale.Add(saleMonth, (sm.Quantity * sm.SalePrice));
                }


            }

            foreach (KeyValuePair<string, decimal> item in monthSale)
            {
                ChartModel cm = new ChartModel(item.Key, item.Value);

                barChart.Add(cm);

            }


            return barChart;
        }

        public List<ChartModel> calculateYearlySalesChart()
        {
            List<ChartModel> barChart = new List<ChartModel>();
            List<SalesModel> barSales = new List<SalesModel>();

            DateTime currentDateTime = DateTime.Now;

            

            //Console.WriteLine("Yer is " + yearAndTime[0]);

            barSales = sales.getSalesUsingYear("20");
            Dictionary<string, decimal> monthSale = new Dictionary<string, decimal>();


            foreach (SalesModel sm in barSales)
            {

                DateTime saleDate = sm.SaleDate;
                string[] saleDateList = sm.SaleDate.ToString().Split('/');

                
                string[] yearAndTime = saleDateList[2].Split(' ');
                string saleYear = yearAndTime[0];


                if (monthSale.TryGetValue(saleYear, out decimal value))
                {
                    value = value + (sm.Quantity * sm.SalePrice);

                }
                else
                {
                    monthSale.Add(saleYear, (sm.Quantity * sm.SalePrice));
                }


            }

            foreach (KeyValuePair<string, decimal> item in monthSale)
            {
                ChartModel cm = new ChartModel(item.Key, item.Value);

                barChart.Add(cm);

            }


            return barChart;
        }


        public List<ChartModel> calculateProductInStock()
        {
            List<ChartModel> productsChart = new List<ChartModel>();

            List<ProductModel> products = new List<ProductModel>();
            products = product.getProducts();
            int totalQuan = 0;

            foreach (ProductModel pm in products)
            {
                
                int quant;
                quant = pm.Quantity;
                totalQuan = totalQuan + quant;
                
            }

            foreach(ProductModel pm in products)
            {
                double percent;

                percent = pm.Quantity * 100 / totalQuan;

                

                productsChart.Add(new ChartModel(pm.Name, percent));
            }

            return productsChart;

        }

        



    }

    public class Combo
    {
        
        public string Choice { get; set; }

        public Combo(string choice)
        {
            Choice = choice;
        }

        public Combo()
        {

        }
    }

    
}
