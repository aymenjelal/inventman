﻿using Caliburn.Micro;
using InventMan.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.ViewModels
{
    class AddProductViewModel : Conductor<object>
    {
        private int _id;
        private string _name;
        private string _brand;
        private int _quantity;
        private decimal _price;
        private DateTime _date;
        private ObservableCollection<ProductModel> _products = new ObservableCollection<ProductModel>();
        ProductModel product = new ProductModel();
        private readonly IEventAggregator _eventAggregator;


        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyOfPropertyChange(() => Id);
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        private string Brand
        {
            get { return _brand; }
            set
            {
                _brand = value;
                NotifyOfPropertyChange(() => Brand);
            }

        }

        private int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                NotifyOfPropertyChange(() => Quantity);
            }
        }

        private decimal Price
        {
            get { return _price; }
            set
            {
                _price = value;
                NotifyOfPropertyChange(() => Price);
            }
        }

        private DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                NotifyOfPropertyChange(() => Date);
            }
        }




        

        //public void AddProduct(string name, string brand, int quantity, decimal price)
        //{

        //    ProductModel pm = new ProductModel();
        //    pm= product.addProduct(name, brand, quantity, price, DateTime.Now);
        //    ProductViewModel pvm = new ProductViewModel();
        //    HomeViewModel hvm = new HomeViewModel();
        //    //List<ProductModel> productList = new List<ProductModel>();
        //    //ObservableCollection<ProductModel> PopProducts = new ObservableCollection<ProductModel>();

        //    //productList = product.getProducts();



        //    //for (int i = 0; i < productList.Count; i++)
        //    //{

        //    //    PopProducts.Add(productList[i]);


        //    //}
        //    //pvm.Products.Add(new ProductModel { Id = 5, Name = "x", Brand = "y", Quantity = 40, Price = 10, Date = DateTime.Now });

        //    //pvm.NotifyOfPropertyChange(pvm.Products=PopProducts);

        //    //pvm.NotifyOfPropertyChange();
            
            
        //    TryClose();

        //}



    }
}
