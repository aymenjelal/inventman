﻿using Caliburn.Micro;
using InventMan.Models;
using InventMan.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.ViewModels
{
    public class LoginViewModel :Screen
    {
        public UserModel user;

        private string _userName;
        private string _password;


        public string  UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyOfPropertyChange(() => Password);
            }
        }


        public void Login(string userName, string passowrd, object sender)
        {
            user = new UserModel();
            user = user.getUser(userName);

            if (user.Password.Equals(this.Password))
            {
                HomeViewModel home = new HomeViewModel(user);
                
                //home.
            }



        }

        public void close()
        {
            TryClose();
        }

    }
}
