﻿using Caliburn.Micro;
using InventMan.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventMan.ViewModels
{
    class PlaceOrderViewModel:Conductor<object>
    {

        private int _id;
        private string _productName;
        private string _productBrand;
        private string  _quantity;
        private string _price;
        private DateTime _orderDate;
        private int _pending;
        private string _tresh;
        private List<ChartModel> _productChart = new List<ChartModel>();

        public ProductModel product = new ProductModel();

        OrderModel order = new OrderModel();

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyOfPropertyChange(() => Id);
            }
        }

        public string ProductName
        {
            get { return _productName; }
            set
            {
                _productName = value;
                NotifyOfPropertyChange(() => ProductName);
            }
        }

        public string ProductBrand
        {
            get { return _productBrand; }
            set
            {
                _productBrand = value;
                NotifyOfPropertyChange(() => ProductBrand);
            }

        }

        public  string  Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                NotifyOfPropertyChange(() => Quantity);
            }
        }

        public string Price
        {
            get { return _price; }
            set
            {
                _price = value;
                NotifyOfPropertyChange(() => Price);
            }
        }

        public string Tresh
        {
            get { return _tresh; }
            set
            {
                _tresh = value;
                NotifyOfPropertyChange(() => Tresh);
            }
        }

        public DateTime OrderDate
        {
            get { return _orderDate; }
            set
            {
                _orderDate = value;
                NotifyOfPropertyChange(() => OrderDate);
            }
        }

        public int Pending
        {
            get { return _pending; }
            set
            {
                _pending = value;
                NotifyOfPropertyChange(() => Pending);
            }
        }

        public List<ChartModel> ProductChart
        {
            get { return _productChart; }
            set
            {
                _productChart = value;
                NotifyOfPropertyChange(() => ProductChart);


            }
        }

        public PlaceOrderViewModel()
        {
            ProductChart = getFourLowProducts();
        }

        public void PlaceOrder(string productName, string productBrand,string quantity,string price, string tresh)
        {
            order.placeOrder(productName, productBrand,int.Parse(quantity), decimal.Parse(price), int.Parse(tresh), DateTime.UtcNow, 1);
            ClearText();
        }

        //public bool CanClearText(string productName, string productBrand, int quantity, decimal price)
        //{
        //    if(string.IsNullOrWhiteSpace(productName)&& string.IsNullOrWhiteSpace(productBrand))
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}

        public void ClearText( )
        {
            ProductBrand = "";
            ProductName = "";
            
            Quantity = "";
            Price = "";
            Tresh = "";
        }

        public ChartModel CalculatePercentage(ProductModel product)
        {
            ChartModel chart = new ChartModel();
            Console.WriteLine("inside Calculate Percentage");

            double percent;
            
            Console.WriteLine("Selected Product is " + product.ToString());
            percent = (product.Quantity - product.Tresh) * 100 / product.Quantity;
            chart.Element = product.Name;
            chart.Percent = percent;
            return chart;



        }

        public List<ChartModel> getLowProducts()
        {
            List<ProductModel> products = product.getProducts();
            List<ChartModel> lowProducts = new List<ChartModel>();

            for (int i = 0; i < products.Count(); i++)
            {
                lowProducts.Add(CalculatePercentage(products[i]));
            }

            return lowProducts;
        }

        public void sortLowProducts(List<ChartModel> lowProducts)
        {
            List<ChartModel> sortedLowProducts = new List<ChartModel>();
            for (int k = 0; k < lowProducts.Count(); k++)
            {
                Console.WriteLine("Sorted Product " + lowProducts[k].Percent);
            }

            for (int i=0; i < lowProducts.Count()-1; i++)
            {
                int min_idx = i;
                for(int j = i + 1; j < lowProducts.Count(); j++)
                {
                    if (lowProducts[j].Percent < lowProducts[min_idx].Percent)
                    {
                        
                        min_idx = j;
                    }
                }

                ChartModel chart = lowProducts[min_idx];
                lowProducts[min_idx] = lowProducts[i];
                lowProducts[i] = chart;

                

            }
            for (int k = 0; k < lowProducts.Count(); k++)
            {
                Console.WriteLine("Sorted Product " + lowProducts[k].Percent);
            }
        }

        public List<ChartModel> getFourLowProducts()
        {
            List<ChartModel> lowProducts = new List<ChartModel>();
            List<ChartModel> lastThree = new List<ChartModel>();


            lowProducts = getLowProducts();
            sortLowProducts(lowProducts);

            if (lowProducts.Count() >= 4)
            {
                for(int i=0; i<4; i++)
                {
                    lastThree.Add(lowProducts[i]);
                }
            }

            else
            {
                lastThree = lowProducts;
            }

            return lastThree;
            
        }




    }
}
